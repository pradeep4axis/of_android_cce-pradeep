//
// Created by Nilupul Sandeepa on 2021-05-28.
//

#ifndef CAMERA_H
#define CAMERA_H

#include "../utils/Macros.h"
#include "../utils/Touch.h"
#include "../utils/Types.h"
#include "../../../../../libs/openFrameworks/types/ofPoint.h"

NS_ADE_BEGIN

class Camera {

    public:
        Camera();
        Camera(int width, int height);

        void setup(int width, int height);
        void begin();
        void end();
        void pan(Touch translate, EventState state);
        void zoom(Touch center, float scale, EventState state);
        ofPoint toCanvasPosition(const Touch& screenPos);
        float getZoom();
        void setMinimumZoom(float minZoom);
        ofPoint getCanvasTranslation();
        ofPoint toTouchPosition(const ofPoint& touchPos);
        ofPoint getZoomOffset();
        ofPoint getPanOffset();
        void setZoom(float zoom);

    protected:
        ofPoint origin;
        int width;
        int height;
        float zoomValue;
        ofPoint originStart;
        ofPoint panOffset;
        ofPoint zoomCenter;
        ofPoint zoomOffset;
        ofPoint zoomOnScreen;
        float zoomStart;
        float minZoom;
        bool isPanning;
        bool isZooming;

        void resetOffsets();
        void restrictOrigin();
        ofPoint toCanvasPosition(const ofPoint& screenPos);
};

NS_ADE_END

#endif //CAMERA_H