#include <jni.h>
#include "ofMain.h"
#include "EngineApp.h"
#include "../../../../libs/openFrameworks/app/ofAppRunner.h"

//USING_NS_ADE

ade::EngineApp* app;
JavaVM* jvm;
JNIEnv* getEnv() {
    JNIEnv* myNewEnv;
    JavaVMAttachArgs args;
    args.version = JNI_VERSION_1_6; // choose your JNI version
    args.name = NULL; // you might want to give the java thread a name
    args.group = NULL; // you might want to assign the java thread to a ThreadGroup
    jvm->AttachCurrentThread(&myNewEnv, &args);
    return myNewEnv;
}

void readyCallback(ade::EventState state) {
    JNIEnv* env = getEnv();
    jclass clazz = env->FindClass("com/axis/cce/CCEApp");
    jmethodID callback = env->GetStaticMethodID(clazz, "onCanvasChange", "(I)V");
    env->CallStaticVoidMethod(clazz, callback, (int)state);
}

int main(){
    ofGLESWindowSettings settings;
    settings.setGLESVersion(3);
    settings.windowMode = OF_FULLSCREEN;
    ofCreateWindow(settings);		// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
    app = new ade::EngineApp();
	ofRunApp(app);
	return 0;
}


#ifdef TARGET_ANDROID
void ofAndroidApplicationInit() {
    //application scope init
}

void ofAndroidActivityInit() {
    //activity scope init
    main();
}

extern "C" {
    void Java_com_axis_cce_CCEApp_nInit(JNIEnv* env, jobject thiz) {
        env->GetJavaVM(&jvm);
        ofLogError("CCEApp") << "Initialization";
        app->setupCanvas(100, 100);
        app->setCanvasCallback(readyCallback);
    }
}
#endif
