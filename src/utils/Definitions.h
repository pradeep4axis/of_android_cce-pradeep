//
// Created by Nilupul Sandeepa on 2021-05-29.
//

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include "Macros.h"
#include "Types.h"

NS_ADE_BEGIN

typedef void (*ChangeCallback) (EventState state);
typedef void (*GLCallback) (bool);
typedef void (*ColorCallback) (float,float,float,float);

NS_ADE_END

#endif //DEFINITIONS_H